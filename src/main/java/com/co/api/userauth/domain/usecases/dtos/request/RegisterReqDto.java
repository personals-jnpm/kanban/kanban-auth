package com.co.api.userauth.domain.usecases.dtos.request;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@RequiredArgsConstructor
public class RegisterReqDto {

    @NotEmpty
    @NotNull
    @Size(min = 2, max = 100)
    private String firstName;

    @NotEmpty
    @NotNull
    @Size(min = 2, max = 100)
    private String lastName;

    @NotEmpty
    @NotNull
    @Size(min = 2, max = 100)
    private String username;

    @Email
    @NotEmpty
    @NotNull
    private String email;
}
