package com.co.api.userauth.domain.models.role;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Data
@RequiredArgsConstructor
public class Role {

    private Long id;
    private String name;
    private boolean isActive;
    private LocalDateTime createdDate;

    public Role(String name) {
        this.name = name;
        this.isActive = true;
    }
}
