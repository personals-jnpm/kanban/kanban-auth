package com.co.api.userauth.domain.exceptions;

public class CustomException extends RuntimeException {

    public CustomException(String message) {
        super(message);
    }

}
