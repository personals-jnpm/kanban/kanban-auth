package com.co.api.userauth.domain.usecases.user;

import com.co.api.userauth.application.security.models.UserDetailsApp;
import com.co.api.userauth.application.utils.EmailUtil;
import com.co.api.userauth.application.utils.LoginAttempts;
import com.co.api.userauth.domain.enums.RoleType;
import com.co.api.userauth.domain.exceptions.CustomException;
import com.co.api.userauth.domain.exceptions.ResourceNotFoundException;
import com.co.api.userauth.domain.mappers.UserMapperDomain;
import com.co.api.userauth.domain.models.role.Role;
import com.co.api.userauth.domain.models.user.User;
import com.co.api.userauth.domain.models.user.gateway.UserRepository;
import com.co.api.userauth.domain.usecases.dtos.request.RegisterReqDto;
import com.co.api.userauth.domain.usecases.dtos.response.UserResDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.mapstruct.factory.Mappers;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;

import static com.co.api.userauth.application.utils.constants.Constants.*;

@RequiredArgsConstructor
@Slf4j
public class UserUseCaseImpl implements UserUseCase, UserDetailsService {

    private final UserRepository repository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final LoginAttempts loginAttempts;
    private final EmailUtil emailUtil;

    private static final UserMapperDomain mapper = Mappers.getMapper(UserMapperDomain.class);

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = repository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format(USER_BY_USERNAME_NOT_FOUND, username)));
        validateLoginAttempts(user);
        user.setLastLoginDisplay(user.getLastLogin());
        user.setLastLogin(LocalDateTime.now());
        return new UserDetailsApp(repository.save(user));
    }

    @Override
    public UserResDto register(RegisterReqDto reqDto) {
        validateUsernameAndEmail(reqDto.getUsername(), reqDto.getEmail());
        return mapper.entityToResponse(repository.save(makeUser(new User(), reqDto.getFirstName(), reqDto.getLastName(),
                reqDto.getUsername(), reqDto.getEmail(), encodePassword(generatePassword(reqDto.getUsername(),
                        reqDto.getEmail())))));
    }

    @Override
    public User findUserByUsername(String username) {
        return repository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(USER_BY_USERNAME_NOT_FOUND, username)));
    }

    private void validateUsernameAndEmail(String username, String email) {
        if (repository.existByUsername(username))
            throw new CustomException(String.format(USERNAME_ALREADY_EXISTS, username));
        if (repository.existByEmail(email)) throw new CustomException(String.format(EMAIL_ALREADY_EXISTS, email));
    }

    private User makeUser(User user, String firstName, String lastName, String username, String email, String passwordEncoded) {
        user.setUserId(generateUserId());
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(passwordEncoded);
        user.setRole(new Role(RoleType.ROLE_USER.name()));
        user.setProfileImageURL(generateDefaultURLProfileImage(username));
        user.setActive(true);
        user.setNotLocked(true);
        user.setJoinDate(LocalDateTime.now());
        return user;
    }

    private String generateUserId() {
        return RandomStringUtils.randomNumeric(10);
    }

    private String generateDefaultURLProfileImage(String username) {
        return String.format(DEFAULT_URL_PROFILE_IMAGE, username);
    }

    private String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    private String generatePassword(String username, String email) {
        String password = RandomStringUtils.randomAlphanumeric(10);
        log.info("Password generated: {} ", password);
        emailUtil.sendNewPasswordEmail(username, password, email);
        return password;
    }

    private void validateLoginAttempts(User user) {
        if (user.isNotLocked()) {
            user.setNotLocked(!loginAttempts.hasExcededMaxAttempts(user.getUsername()));
        } else {
            loginAttempts.clearUserFromLoginAttemptsCache(user.getUsername());
        }
    }
}
