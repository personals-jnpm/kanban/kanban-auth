package com.co.api.userauth.domain.enums;

public enum RoleType {

    ROLE_USER(),
    ROLE_HR(),
    ROLE_MANAGER(),
    ROLE_ADMIN(),
    ROLE_SUPER_ADMIN();

}
