package com.co.api.userauth.domain.models.user;

import com.co.api.userauth.domain.models.role.Role;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Data
@RequiredArgsConstructor
public class User {

    private Long id;
    private String userId;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String email;
    private String profileImageURL;
    private boolean isActive;
    private boolean isNotLocked;
    private Role role;
    private LocalDateTime lastLogin;
    private LocalDateTime lastLoginDisplay;
    private LocalDateTime joinDate;
}
