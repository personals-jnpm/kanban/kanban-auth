package com.co.api.userauth.domain.models.user.gateway;

import com.co.api.userauth.domain.models.user.User;

import java.util.Optional;

public interface UserRepository {

    User save(User user);

    Optional<User> findByUsername(String username);

    boolean existByUsername(String username);

    boolean existByEmail(String email);

}
