package com.co.api.userauth.domain.mappers;

import com.co.api.userauth.domain.models.user.User;
import com.co.api.userauth.domain.usecases.dtos.response.UserResDto;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapperDomain {

    UserResDto entityToResponse(User entity);

}
