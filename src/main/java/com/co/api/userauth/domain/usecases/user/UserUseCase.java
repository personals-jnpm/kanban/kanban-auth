package com.co.api.userauth.domain.usecases.user;

import com.co.api.userauth.domain.models.user.User;
import com.co.api.userauth.domain.usecases.dtos.request.RegisterReqDto;
import com.co.api.userauth.domain.usecases.dtos.response.UserResDto;

public interface UserUseCase {

    UserResDto register(RegisterReqDto reqDto);

    User findUserByUsername(String username);

}
