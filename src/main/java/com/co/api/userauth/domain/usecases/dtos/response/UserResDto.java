package com.co.api.userauth.domain.usecases.dtos.response;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class UserResDto {

    private String userId;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private String profileImageURL;
}
