package com.co.api.userauth.infrastructure.drivenadapters.user;

import com.co.api.userauth.domain.exceptions.ResourceNotFoundException;
import com.co.api.userauth.domain.models.user.User;
import com.co.api.userauth.domain.models.user.gateway.UserRepository;
import com.co.api.userauth.infrastructure.drivenadapters.mappers.UserMapper;
import com.co.api.userauth.infrastructure.drivenadapters.role.RoleDataJPA;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;

import java.util.Optional;

import static com.co.api.userauth.application.utils.constants.Constants.ROLE_OR_AUTHORITY_NOT_FOUND;

@RequiredArgsConstructor
public class UserDataRepository implements UserRepository {

    private final UserDataJPA repositoryJPA;
    private final RoleDataJPA roleDataJPA;
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    @Override
    public User save(User user) {
        UserData userData = userMapper.entityToData(user);
        userData.setRole(roleDataJPA.findByName(user.getRole().getName())
                .orElseThrow(() -> new ResourceNotFoundException(ROLE_OR_AUTHORITY_NOT_FOUND)));
        return userMapper.dataToEntity(repositoryJPA.save(userData));
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return repositoryJPA.findByUsername(username)
                .map(userMapper::dataToEntity);
    }

    @Override
    public boolean existByUsername(String username) {
        return repositoryJPA.existsByUsername(username);
    }

    @Override
    public boolean existByEmail(String email) {
        return repositoryJPA.existsByEmail(email);
    }

}
