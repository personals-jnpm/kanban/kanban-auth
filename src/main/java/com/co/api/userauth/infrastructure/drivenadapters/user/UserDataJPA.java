package com.co.api.userauth.infrastructure.drivenadapters.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserDataJPA extends JpaRepository<UserData, Long> {

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    Optional<UserData> findByUsername(String username);

    Optional<UserData> findByEmail(String email);
}
