package com.co.api.userauth.infrastructure.drivenadapters.role;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleDataJPA extends JpaRepository<RoleData, Long> {

    Optional<RoleData> findByName(String name);
}
