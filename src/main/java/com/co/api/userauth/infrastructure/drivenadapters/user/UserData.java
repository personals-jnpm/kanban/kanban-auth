package com.co.api.userauth.infrastructure.drivenadapters.user;

import com.co.api.userauth.infrastructure.drivenadapters.role.RoleData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "user_auth")
public class UserData implements Serializable {
    @Id
    @Column(name = "id", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String userId;

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    private String email;

    private String profileImageURL;

    private boolean isActive;

    private boolean isNotLocked;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "role_id")
    private RoleData role;

    @LastModifiedDate
    @Column(name = "last_login")
    private LocalDateTime lastLogin;

    @LastModifiedDate
    @Column(name = "last_login_display")
    private LocalDateTime lastLoginDisplay;

    @CreatedDate
    @Column(name = "join_date")
    private LocalDateTime joinDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UserData userData = (UserData) o;
        return id != null && Objects.equals(id, userData.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
