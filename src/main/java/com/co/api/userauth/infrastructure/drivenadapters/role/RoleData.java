package com.co.api.userauth.infrastructure.drivenadapters.role;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "roles")
public class RoleData implements Serializable {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;

    private boolean isActive;

    @CreatedDate
    @Column(name = "created_date", nullable = false)
    private LocalDateTime createdDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        RoleData roleData = (RoleData) o;
        return id != null && Objects.equals(id, roleData.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
