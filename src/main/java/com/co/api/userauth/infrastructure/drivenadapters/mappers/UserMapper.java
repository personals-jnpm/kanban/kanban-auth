package com.co.api.userauth.infrastructure.drivenadapters.mappers;

import com.co.api.userauth.domain.models.user.User;
import com.co.api.userauth.infrastructure.drivenadapters.user.UserData;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    User dataToEntity(UserData userData);

    UserData entityToData(User userEntity);

    List<User> listDataToEntity(List<UserData> userDataList);

}
