package com.co.api.userauth.infrastructure.entrypoints.auth;

import com.co.api.userauth.application.security.JwtTokenProvider;
import com.co.api.userauth.application.security.models.UserDetailsApp;
import com.co.api.userauth.domain.mappers.UserMapperDomain;
import com.co.api.userauth.domain.models.user.User;
import com.co.api.userauth.domain.usecases.dtos.request.LoginReqDto;
import com.co.api.userauth.domain.usecases.dtos.request.RegisterReqDto;
import com.co.api.userauth.domain.usecases.dtos.response.UserResDto;
import com.co.api.userauth.domain.usecases.user.UserUseCase;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.co.api.userauth.application.utils.constants.SecurityConstants.JWT_TOKEN_HEADER;

@RestController
@RequestMapping(path = {"/api"})
@RequiredArgsConstructor
@Validated
public class AuthController {

    private final UserUseCase userUseCase;
    private final AuthenticationManager authenticationManagerBean;
    private final JwtTokenProvider tokenProvider;

    private final UserMapperDomain mapper = Mappers.getMapper(UserMapperDomain.class);

    @PostMapping("/login")
    public ResponseEntity<UserResDto> login(@Valid @RequestBody LoginReqDto reqDto) {
        authenticate(reqDto.getUsername(), reqDto.getPassword());
        User userLogin = userUseCase.findUserByUsername(reqDto.getUsername());
        return new ResponseEntity<>(mapper.entityToResponse(userLogin),
                getJwtHeader(new UserDetailsApp(userLogin)), HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<UserResDto> register(@Valid @RequestBody RegisterReqDto reqDto) {
        return new ResponseEntity<>(userUseCase.register(reqDto), HttpStatus.CREATED);
    }

    private void authenticate(String username, String password) {
        authenticationManagerBean.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }

    private HttpHeaders getJwtHeader(UserDetailsApp user) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(JWT_TOKEN_HEADER, tokenProvider.generateToken(user));
        return headers;
    }
}
