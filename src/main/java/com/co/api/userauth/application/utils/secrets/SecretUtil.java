package com.co.api.userauth.application.utils.secrets;

import com.co.api.userauth.application.utils.DotenvUtil;
import com.google.gson.Gson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueResponse;

@Configuration
public class SecretUtil {
    private static final String DB_LOCAL = "DB_LOCAL";

    @Bean
    @Profile("local")
    public static SecretDBModel getSecretLocal() {
        Gson gson = new Gson();
        String dbStr = DotenvUtil.get(DB_LOCAL);
        return gson.fromJson(dbStr, SecretDBModel.class);
    }

    @Bean
    @Profile("!local")
    public static SecretDBModel getSecret() {
        SecretsManagerClient client = SecretsManagerClient.builder()
                .region(Region.of(DotenvUtil.get("awsRegion")))
                .build();

        GetSecretValueResponse getSecretValueResponse = client.getSecretValue(
                GetSecretValueRequest.builder()
                        .secretId(DotenvUtil.get("secretName"))
                        .build());

        return new Gson().fromJson(
                getSecretValueResponse.secretString(), SecretDBModel.class
        );
    }

}
