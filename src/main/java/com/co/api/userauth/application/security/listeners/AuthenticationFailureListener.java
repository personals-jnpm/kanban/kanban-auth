package com.co.api.userauth.application.security.listeners;

import com.co.api.userauth.application.utils.LoginAttempts;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class AuthenticationFailureListener {

    private final LoginAttempts loginAttempts;

    @EventListener
    public void onAuthenticationFailure(AuthenticationFailureBadCredentialsEvent event) {
        Object principal = event.getAuthentication().getPrincipal();
        if (principal instanceof String) loginAttempts.addUserToLoginAttemptCache((String) principal);
    }
}
