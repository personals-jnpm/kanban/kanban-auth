package com.co.api.userauth.application.utils;

import io.github.cdimascio.dotenv.Dotenv;

public class DotenvUtil {

    private static DotenvUtil dotEnvUtil;
    private final Dotenv dotenv;

    private DotenvUtil() {
        dotenv = Dotenv.configure()
                .directory("./docker")
                .ignoreIfMalformed()
                .filename("env")
                .ignoreIfMissing()
                .load();
    }

    public static Dotenv getInstance() {
        if (dotEnvUtil == null) {
            dotEnvUtil = new DotenvUtil();
        }
        return dotEnvUtil.getDotenv();
    }

    private Dotenv getDotenv() {
        return dotenv;
    }

    public static String get(String key) {
        return DotenvUtil.getInstance().get(key);
    }

}
