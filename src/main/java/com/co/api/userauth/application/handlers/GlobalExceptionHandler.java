package com.co.api.userauth.application.handlers;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.co.api.userauth.domain.exceptions.*;
import com.co.api.userauth.domain.usecases.dtos.ExceptionResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.LockedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex) {
        String message = "Ha ocurrido un error";
        log.error(ex.getMessage());

        return new ResponseEntity<>(
                new ExceptionResponse("INTERNAL_SERVER_ERROR", message),
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        List<String> details = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            details.add("El campo " + error.getField() + " " + error.getDefaultMessage());
        }

        return new ResponseEntity<>(
                new ExceptionResponse("BAD_REQUEST", details),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ExceptionResponse> notReadableException(HttpMessageNotReadableException ex) {
        String message = "Request mal formada";
        return new ResponseEntity<>(
                new ExceptionResponse("BAD_REQUEST", message),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ExceptionResponse> resourceNotFound(ResourceNotFoundException ex) {
        return new ResponseEntity<>(
                new ExceptionResponse("NOT_FOUND", ex.getMessage()),
                HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(ResourceAlreadyExists.class)
    public ResponseEntity<ExceptionResponse> resourceAlreadyExists(ResourceAlreadyExists ex) {
        return new ResponseEntity<>(
                new ExceptionResponse("CONFLICT", ex.getMessage()),
                HttpStatus.CONFLICT
        );
    }

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<ExceptionResponse> customException(CustomException ex) {
        return new ResponseEntity<>(
                new ExceptionResponse("BAD_REQUEST", ex.getMessage()),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<ExceptionResponse> unauthorizedException(UnauthorizedException ex) {
        return new ResponseEntity<>(
                new ExceptionResponse("UNAUTHORIZED", ex.getMessage()),
                HttpStatus.UNAUTHORIZED
        );
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<ExceptionResponse> unauthorizedException(NoHandlerFoundException ex) {
        String message = "La página no ha sido encontrada";
        return new ResponseEntity<>(
                new ExceptionResponse("NOT_FOUND", message),
                HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(JWTVerificationException.class)
    public ResponseEntity<ExceptionResponse> jwtVerificationException(JWTVerificationException ex) {
        String message = "Token no válido";
        return new ResponseEntity<>(
                new ExceptionResponse("CONFLICT", message),
                HttpStatus.CONFLICT
        );
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<ExceptionResponse> forbiddenException(ForbiddenException ex) {
        return new ResponseEntity<>(
                new ExceptionResponse("FORBIDDEN", ex.getMessage()),
                HttpStatus.FORBIDDEN
        );
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ExceptionResponse> authenticationException(BadCredentialsException ex) {
        String message = "Las credenciales son incorrectas";
        return new ResponseEntity<>(
                new ExceptionResponse("BAD_REQUEST", message),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(InternalAuthenticationServiceException.class)
    public ResponseEntity<ExceptionResponse> authenticationException(InternalAuthenticationServiceException ex) {
        return new ResponseEntity<>(
                new ExceptionResponse("CONFLICT", ex.getMessage()),
                HttpStatus.CONFLICT
        );
    }

    @ExceptionHandler(LockedException.class)
    public ResponseEntity<ExceptionResponse> authenticationException(LockedException ex) {
        String message = "Tu cuenta ha sido bloqueada";
        return new ResponseEntity<>(
                new ExceptionResponse("LOCKED", message),
                HttpStatus.LOCKED
        );
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ExceptionResponse> authenticationException(AccessDeniedException ex) {
        String message = "No tienes permiso para acceder a este sitio";
        return new ResponseEntity<>(
                new ExceptionResponse("UNAUTHORIZED", message),
                HttpStatus.UNAUTHORIZED
        );
    }

    @ExceptionHandler(DisabledException.class)
    public ResponseEntity<ExceptionResponse> authenticationException(DisabledException ex) {
        String message = "El usuario está deshabilitado";
        return new ResponseEntity<>(
                new ExceptionResponse("CONFLICT", message),
                HttpStatus.CONFLICT
        );
    }
}
