package com.co.api.userauth.application.security.listeners;

import com.co.api.userauth.application.security.models.UserDetailsApp;
import com.co.api.userauth.application.utils.LoginAttempts;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AuthenticationSuccessListener {

    private final LoginAttempts loginAttempts;

    @EventListener
    public void onAuthenticationSuccess(AuthenticationSuccessEvent event) {
        Object principal = event.getAuthentication().getPrincipal();
        if (principal instanceof UserDetailsApp)
            loginAttempts.clearUserFromLoginAttemptsCache(((UserDetailsApp) principal).getUsername());
    }
}
