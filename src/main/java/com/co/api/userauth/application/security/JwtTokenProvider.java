package com.co.api.userauth.application.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.co.api.userauth.application.security.models.UserDetailsApp;
import com.co.api.userauth.application.utils.DotenvUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import static com.co.api.userauth.application.utils.constants.SecurityConstants.*;

@Component
public class JwtTokenProvider {

    public String generateToken(UserDetailsApp userDetails) {
        return JWT.create().withIssuer(ARRAYS_LLC)
                .withAudience(ARRAYS_ADMINISTRATION)
                .withIssuedAt(new Date())
                .withSubject(userDetails.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(DotenvUtil.get("jwtSecret").getBytes(StandardCharsets.UTF_8)));
    }

    public Authentication getAuthentication(String username, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken
                (username, null, null);
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        return authenticationToken;
    }

    public boolean isTokenValid(String token) {
        return !getJwtVerifier().verify(token).getExpiresAt().before(new Date());
    }

    public String getSubject(String token) {
        return getJwtVerifier().verify(token).getSubject();
    }

    private JWTVerifier getJwtVerifier() {
        return JWT.require(Algorithm.HMAC512(DotenvUtil.get("jwtSecret")))
                .withIssuer(ARRAYS_LLC)
                .build();
    }

}
