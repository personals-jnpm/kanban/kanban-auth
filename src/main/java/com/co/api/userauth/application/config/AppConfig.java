package com.co.api.userauth.application.config;

import com.co.api.userauth.application.utils.EmailUtil;
import com.co.api.userauth.application.utils.LoginAttempts;
import com.co.api.userauth.domain.models.user.gateway.UserRepository;
import com.co.api.userauth.domain.usecases.user.UserUseCase;
import com.co.api.userauth.domain.usecases.user.UserUseCaseImpl;
import com.co.api.userauth.infrastructure.drivenadapters.role.RoleDataJPA;
import com.co.api.userauth.infrastructure.drivenadapters.user.UserDataJPA;
import com.co.api.userauth.infrastructure.drivenadapters.user.UserDataRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Collections;

import static com.co.api.userauth.application.utils.constants.SecurityConstants.*;

@Configuration
public class AppConfig {

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public LoginAttempts loginAttempts() {
        return new LoginAttempts();
    }

    @Bean
    public EmailUtil emailUtil() {
        return new EmailUtil();
    }

    @Bean
    public UserRepository userRepository(UserDataJPA userDataJPA, RoleDataJPA roleDataJPA) {
        return new UserDataRepository(userDataJPA, roleDataJPA);
    }

    @Bean
    public UserDetailsService userDetailsService(UserRepository repository, BCryptPasswordEncoder passwordEncoder,
                                                 LoginAttempts loginAttempts, EmailUtil emailUtil) {
        return new UserUseCaseImpl(repository, passwordEncoder, loginAttempts, emailUtil);
    }

    @Bean
    public UserUseCase userUseCase(UserRepository repository, BCryptPasswordEncoder passwordEncoder,
                                   LoginAttempts loginAttempts, EmailUtil emailUtil) {
        return new UserUseCaseImpl(repository, passwordEncoder, loginAttempts, emailUtil);
    }

    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(SHOW_CREDENTIALS);
        corsConfiguration.setAllowedOrigins(Collections.singletonList(ALLOWED_ORIGINS));
        corsConfiguration.setAllowedHeaders(ALLOWED_HEADERS);
        corsConfiguration.setExposedHeaders(EXPOSED_HEADERS);
        corsConfiguration.setAllowedMethods(ALLOWED_METHODS);

        UrlBasedCorsConfigurationSource basedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        basedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(basedCorsConfigurationSource);
    }

}
