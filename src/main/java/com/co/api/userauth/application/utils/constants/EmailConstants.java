package com.co.api.userauth.application.utils.constants;

import com.co.api.userauth.application.utils.DotenvUtil;

public class EmailConstants {

    public static final String SIMPLE_MAIL_TRANSFER_PROTOCOL = "smtps";
    public static final String USERNAME = DotenvUtil.get("userNameEmail");
    public static final String PASSWORD = DotenvUtil.get("passwordEmail");
    public static final String FROM_EMAIL = DotenvUtil.get("userNameEmail");
    public static final String CC_EMAIL = "";
    public static final String EMAIL_SUBJECT = "API Authentication - Nueva contraseña";
    public static final String GMAIL_SMTP_SERVER = "smtp.gmail.com";
    public static final String SMTP_HOST = "mail.smtp.host";
    public static final String SMTP_AUTH = "mail.smtp.auth";
    public static final String SMTP_PORT = "mail.smtp.port";
    public static final int DEFAULT_PORT = 465;
    public static final String SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
    public static final String SMTP_STARTTLS_REQUIRED = "mail.smtp.starttls.required";

    public static final String MESSAGE_TEXT = "Hola %s , \n \nLa contraseña de tu cuenta es: %s \n \nEquipo de soporte";
}
