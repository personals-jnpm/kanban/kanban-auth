package com.co.api.userauth.application.utils.secrets;

import lombok.Data;

@Data
public class SecretDBModel {
    private String username;
    private String password;
    private String host;
    private String port;
    private String dbname;
    private String engine;
    private String dbInstanceIdentifier;

}