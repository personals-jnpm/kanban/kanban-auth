package com.co.api.userauth.application.utils.constants;

import java.util.List;

public class SecurityConstants {

    public static final long EXPIRATION_TIME = 432_000_000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String JWT_TOKEN_HEADER = "Jwt-Token";
    public static final String ARRAYS_LLC = "Auth api";
    public static final String ARRAYS_ADMINISTRATION = "User Management Portal";
    public static final String AUTHORITIES = "authorities";
    public static final String FORBIDDEN_MESSAGE = "Necesitas iniciar sesión para acceder a este recurso";
    public static final String ACCESS_DENIED_MESSAGE = "No tienes permiso para acceder a este sitio";
    public static final String OPTION_HTTP_METHOD = "OPTIONS";
    public static final String[] PUBLIC_URLS = {"/api/login", "/api/register"};

    public static final boolean SHOW_CREDENTIALS = true;
    public static final String ALLOWED_ORIGINS = "http://localhost:4200";
    public static final List<String> ALLOWED_HEADERS = List.of("Origin", "Access-Control-Allow-Origin", "Content-Type",
            "Accept", "Jwt-Token", "Authorization", "Origin, Accept", "Access-Control-Request-Method",
            "X-Request-With", "Access-Control-Request-Headers", "Access-Control-Allow-Origin");
    public static final List<String> EXPOSED_HEADERS = List.of("Origin", "Content-Type", "Accept", "Jwt-Token",
            "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials");
    public static final List<String> ALLOWED_METHODS = List.of("GET", "POST", "DELETE", "OPTIONS");
}
