package com.co.api.userauth.application.utils.constants;

public class Constants {

    public static final String DEFAULT_URL_PROFILE_IMAGE = "https://robohash.org/%s";

    public static final String USER_BY_USERNAME_NOT_FOUND = "El usuario con nombre de usuario %s no ha sido encontrado";
    public static final String USERNAME_ALREADY_EXISTS = "El nombre de usuario %s ya se encuentra registrado";
    public static final String CURRENT_USERNAME_NOT_EXISTS = "El nombre de usuario actual %s no se encuentra registrado";
    public static final String EMAIL_ALREADY_EXISTS = "El correo %s ya se encuentra registrado";

    public static final String USER_DISABLED_SUCCESSFULLY = "El usuario %s ha sido deshabilitado correctamente";

    public static final String ROLE_OR_AUTHORITY_NOT_FOUND = "Ha ocurrido un problema al cargar los permisos de usuario";
}
