INSERT INTO public.roles (id, created_date, isactive, name) VALUES (1, '2022-01-02 15:36:49.000000', true, 'ROLE_USER');
INSERT INTO public.roles (id, created_date, isactive, name) VALUES (2, '2022-01-02 15:36:52.000000', true, 'ROLE_HR');
INSERT INTO public.roles (id, created_date, isactive, name) VALUES (3, '2022-01-02 15:36:53.000000', true, 'ROLE_MANAGER');
INSERT INTO public.roles (id, created_date, isactive, name) VALUES (4, '2022-01-02 15:36:54.000000', true, 'ROLE_ADMIN');
INSERT INTO public.roles (id, created_date, isactive, name) VALUES (5, '2022-01-02 15:36:55.000000', true, 'ROLE_SUPER_ADMIN');

INSERT INTO public.user_auth (id, email, firstname, isactive, isnotlocked, join_date, last_login, last_login_display, lastname, password, profileimageurl, userid, username, role_id) VALUES (0, 'admin408@gmail.com', 'Super', true, true, '2022-01-05 13:02:33.044595', null, null, 'Admin', '$2a$10$9ZF79aCzaAi3iwzUBS6OMOtmz/ajGfF23m/7PZu/1Xxq85faGTHYe', 'https://robohash.org/super-admin', '6362042140', 'super-admin', 5);